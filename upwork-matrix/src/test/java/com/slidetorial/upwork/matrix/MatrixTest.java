/**
 *
 */
package com.slidetorial.upwork.matrix;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import java.util.Random;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.google.common.testing.NullPointerTester;
import nl.jqno.equalsverifier.EqualsVerifier;

/**
 * @author goobar
 *
 */
@SuppressWarnings("javadoc")
public class MatrixTest
{

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void should_Pass_EqualsTests() throws Exception
	{
		EqualsVerifier.forClass(InPlaceMatrix.class).usingGetClass()
			.verify();
	}

	@Test
	public void should_Pass_NullTests() throws Exception
	{
		NullPointerTester tester = new NullPointerTester();
		tester.testAllPublicConstructors(InPlaceMatrix.class);
		tester.testAllPublicInstanceMethods(instance());
	}

	@Test
	public void should_Rotate_Example1() throws Exception
	{
		// given
		Matrix matrix = new InPlaceMatrix(new int[][] {
			// row 0
			{ 1, 2, 3 },
			// row 1
			{ 4, 5, 6 },
			// row 2
			{ 7, 8, 9 } });
		Matrix expectedMatrix = new InPlaceMatrix(new int[][] {
			// row 0
			{ 7, 4, 1 },
			// row 1
			{ 8, 5, 2 },
			// row 2
			{ 9, 6, 3 } });

		// when
		Matrix rotatedMatrix = matrix.rotate();

		// then
		assertThat(rotatedMatrix).isEqualTo(expectedMatrix);
	}

	@Test
	public void should_Rotate_Example2() throws Exception
	{
		// given
		Matrix matrix = new InPlaceMatrix(new int[][] {
			// row 0
			{ 5, 1, 9, 11 },
			// row 1
			{ 2, 4, 8, 10 },
			// row 2
			{ 13, 3, 6, 7 },
			// row 3
			{ 15, 14, 12, 16 } });
		Matrix expectedMatrix = new InPlaceMatrix(new int[][] {
			// row 0
			{ 15, 13, 2, 5 },
			// row 1
			{ 14, 3, 4, 1 },
			// row 2
			{ 12, 6, 8, 9 },
			// row 3
			{ 16, 7, 10, 11 } });

		// when
		Matrix rotatedMatrix = matrix.rotate();

		// then
		assertThat(rotatedMatrix).isEqualTo(expectedMatrix);
	}

	@Test
	public void should_Rotate_Random1x1() throws Exception
	{
		// given
		Matrix matrix = new InPlaceMatrix(
			new int[][] { { randomInteger() } });

		// when
		Matrix rotatedMatrix = matrix.rotate();

		// then
		assertThat(rotatedMatrix).isEqualTo(matrix);
	}

	@Test
	public void should_Rotate_Random2x2() throws Exception
	{
		// given
		int firstElement = randomInteger();
		int secondElement = randomInteger();
		int thirdElement = randomInteger();
		int fourthElement = randomInteger();
		Matrix matrix = new InPlaceMatrix(new int[][] {
			// row 0
			{ firstElement, secondElement },
			// row 1
			{ thirdElement, fourthElement } });
		Matrix expectedMatrix = new InPlaceMatrix(new int[][] {
			// row 0
			{ thirdElement, firstElement },
			// row 1
			{ fourthElement, secondElement } });

		// when
		Matrix rotatedMatrix = matrix.rotate();

		// then
		assertThat(rotatedMatrix).isEqualTo(expectedMatrix);
	}

	@Test
	public void should_ToString_ReturnNonEmptyString() throws Exception
	{
		assertFalse(instance().toString().isEmpty());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception
	{
	}

	/**
	 * @return
	 */
	private Matrix instance()
	{
		return new InPlaceMatrix(new int[][] { { 1 } });
	}

	/**
	 * @return
	 */
	private int randomInteger()
	{
		return new Random().nextInt(999);
	}
}
