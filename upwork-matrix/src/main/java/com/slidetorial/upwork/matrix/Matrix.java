/**
 *
 */
package com.slidetorial.upwork.matrix;

/**
 * Represents a 2D matrix with arbitrary number of rows and columns.
 *
 * @author goobar
 *
 */
public interface Matrix
{
	/**
	 * Creates a new matrix rotated by 90 degrees clockwise.
	 *
	 * @return the new matrix rotated by 90 degrees clockwise
	 */
	Matrix rotate();
}
