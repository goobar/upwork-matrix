/**
 *
 */
package com.slidetorial.upwork.matrix;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Arrays;
import org.apache.commons.lang3.ArrayUtils;

/**
 * This implementation uses the following mathematical fact: to rotate a 2D
 * matrix it's enough to first transpose it (
 * <a href="https://en.wikipedia.org/wiki/Transpose">flip over the diagonal</a>)
 * and then simply reflect through axis (vertical if clockwise or horizontal if
 * counter-clockwise). The reflect operation is performed in-place. However,
 * still a new matrix is returned to conform to the contract of the implemented
 * interface.
 *
 * @author goobar
 *
 */
public class InPlaceMatrix implements Matrix
{

	private static void reflectVerticallyInPlace(InPlaceMatrix matrix)
	{
		int rowDim = matrix.content.length;
		int colDim = matrix.content[0].length;
		for (int row = 0; row < rowDim; row++)
		{
			for (int col = 0; col < colDim / 2; col++)
			{
				ArrayUtils.swap(matrix.content[row], col,
					colDim - col - 1);
			}
		}
	}

	private final int[][] content;

	/**
	 * @param content
	 *                content, the first dimension represent rows, the
	 *                second are columns
	 * @throws NullPointerException
	 *                 if any argument is null
	 */
	public InPlaceMatrix(int[][] content) throws NullPointerException
	{
		validate(content);
		this.content = Arrays.copyOf(content, content.length);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		InPlaceMatrix other = (InPlaceMatrix) obj;
		if (!Arrays.deepEquals(content, other.content))
		{
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(content);
		return result;
	}

	@Override
	public Matrix rotate()
	{
		return rotateMatrix();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		final int maxLen = 10;
		return "InPlaceMatrix [content="
			+ (content != null
				? Arrays.asList(content).subList(0,
					Math.min(content.length, maxLen))
				: null)
			+ "]";
	}

	private InPlaceMatrix rotateMatrix()
	{
		InPlaceMatrix result = transpose();
		reflectVerticallyInPlace(result);
		return result;
	}

	private InPlaceMatrix transpose()
	{
		int rowDim = content.length;
		int colDim = content[0].length;
		InPlaceMatrix transposedMatrix = new InPlaceMatrix(
			new int[colDim][rowDim]);
		for (int row = 0; row < rowDim; row++)
		{
			for (int col = 0; col < colDim; col++)
			{
				transposedMatrix.content[col][row] = content[row][col];
			}
		}
		return transposedMatrix;
	}

	/**
	 * @param content
	 */
	private void validate(int[][] content)
	{
		checkNotNull(content, "'content' cannot be null");
	}
}
